"""
Модуль для работы с данными открытых реестров Роспатента (ФИПС).
Модуль работает с реестрами Изобретений, Полезных моделей,
Промышленных образцов, Товарных знаков, Заявок на регистрацию Товарноых знаков
"""

# version 1.0
# author Ilya Temerev (kidmoto)
# email kidmoto@yandex.ru

import requests
from bs4 import BeautifulSoup as BS


class GetHTML:
    """
    Класс для получения данных с сайта ФИПС в виде html-страницы
    """
    user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64)AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.102 Safari/537.36 Edg/104.0.1293.63'  # noqa

    def __init__(self, number: int, casetype: str):
        """
        Экземпляр класса принимает в качестве аргументов номер патента
        'number' в виде числа и тип объекта интеллектуальной
        собственности 'casetype' в виде строки 'PAT' для изобретения,
        "DE" для Промышленного образца, "PM" для полезной модели или
        "TM" для товарного знака
        """

        self.number = number
        self.casetype = casetype

    def check_casetype(self) -> bool:
        """
        Проверяет, имеет ли тип объекта ИС 'casetype' верное значение
        """

        return self.casetype.upper() in ['PAT', 'DE', 'PM', 'TM', 'TMAP']

    def get_url(self) -> str:
        """
        Формирует ссылку в виде строки исходя из номера и типа
        объекта ИС
        """

        return f'https://www1.fips.ru/registers-doc-view/fips_servlet?DB=RU{self.casetype}&DocNumber={str(self.number)}&TypeFile=html'  # noqa

    def get_response(self, url: str) -> requests.models.Response:
        """
        Принимает в качестве аргумента ссылку сайта ФИПС на объект ИС
        (str). Отправляет get-запрос на сайт ФИПС и возвращает
        html-страницу с данными объекта ИС. При отсутсвии User-Agent-а
        раскомментировать следущую строку кода и закоментировать
        последние две
        """

        # return requests.get(url)

        headers = {'User-Agent': self.user_agent}
        return requests.get(url, headers=headers)

    def get_soup(self, response: requests.models.Response) -> BS:
        """
        Принимает в качестве аргумента объъект Response и возвращает
        объект soup для последующего парсинга (сбора данных с
        html-страницы)
        """

        return BS(response.content, 'html.parser')


class Patent(GetHTML):
    """
    Класс для получения информации со страницы Изобретения или Полезной Модели 
    """

    def __init__(self, *args):
        super().__init__(*args)
        
        self.check_casetype()
        self.url = self.get_url()
        self.response = self.get_response(self.url)
        self.soup = self.get_soup(self.response)

    def get_status(self) -> str:
        """
        Возвращает статус
        """

        info = self.soup.find('td', id='StatusR').text.strip()
        info_list = info.split('\n\t')
        return info_list[0].strip()
    
    def get_taxyear(self) -> str:
        """
        Возвращает год оплаты последней пошлины 
        """

        info_list = self.soup.find_all('td', id='StatusR')
        info = info_list[1].text.strip()
        index_za = info.index('за')

        if info[index_za + 4].isdigit():
            pat_year = info[index_za + 3:index_za + 5]
        else:
            pat_year = info[index_za + 3]
        return pat_year
    
    def get_application(self) -> str:
        """
        Возвращает номер заявки 
        """

        for info in self.soup.find('table', id='bib').find_all('p'):
            if '(21)' in info.text:
                return int(info.text.strip().split()[2][:10])


class Design(GetHTML):
    """
    Класс для получения информации со страницы Промышленного образца 
    """

    def __init__(self, *args):
        super().__init__(*args)
        
        self.check_casetype()
        self.url = self.get_url()
        self.response = self.get_response(self.url)
        self.soup = self.get_soup(self.response)

    def get_status(self) -> str:
        """
        Возвращает статус
        """

        info = self.soup.find('td', id='StatusR').text.strip()
        info_list = info.split('\n\t')
        return info_list[0].strip()
    
    def get_taxyear(self) -> str:
        """
        Возвращает год оплаты последней пошлины 
        """

        info_list = self.soup.find('td', id='StatusR').text.split('\n\t')
        info = info_list[2].strip()
        index_za = info.index('за')

        if info[index_za + 4].isdigit():
            pat_year = info[index_za + 3:index_za + 5]
        else:
            pat_year = info[index_za + 3]
        return pat_year

    def get_application(self) -> str:
        """
        Возвращает номер заявки 
        """

        for info in self.soup.find('td', id='td2').find_all('a'):
            if len(info.text) == 10 and info.text.isdigit():
                return info.text


class TradeMark(GetHTML):
    """
    Класс для получения информации со страницы Товарного знака
    """

    def __init__(self, *args):
        super().__init__(*args)
        
        self.check_casetype()
        self.url = self.get_url()
        self.response = self.get_response(self.url)
        self.soup = self.get_soup(self.response)

    def get_imagelink(self) -> str:
        """
        Возвращает ссылку на изображение товарного знака
        """

        for i in self.soup.find_all('a', target='_blank'):
            if 'jpg' in i.get('href').lower():
                return i.get('href')

    def get_applicationdate(self) -> str:
        """
        Возвращает дату подачи заявки
        """

        for info in self.soup.find('td', id='BibR').find_all('p'):
            if '(220)' in info.text:
                return info.text.strip()[26:]

    def get_registrationdate(self) -> str:
        """
        Возвращает дату регистрации
        """

        for info in self.soup.find('td', id='BibR').find_all('p'):
            if '(151)' in info.text:
                return info.text.strip()[24:]

    def get_holder(self) -> str:
        """
        Возвращает имя правообладателя и его адрес
        """

        holder = []
        for info in self.soup.find_all('p', class_='bib'):
            if '(732)' in info.text:
                holder.append(info.text.strip())
        return holder[-1][6:].replace('\n\n', ' ')
    
    def get_representative(self) -> str:
        """
        Возвращает имя представителя и его адрес (адрес для переписки)
        """

        holder = []
        for info in self.soup.find_all('p', class_='bib'):
            if '(750)' in info.text:
                holder.append(info.text.strip())
        return holder[-1][6:].replace('\n\n', ' ')

    def get_MKTU(self) -> tuple:
        """
        Возвращает кортеж из развернутого списка классов МКТУ и списка
        номеров классов, в отношении которых зарегистрирован товарный
        знак
        """

        temp = self.soup.find_all('p', class_='bib')
        classes = []
        for mktu in [c for c in temp if '(511)' in c.text][0].find_all('b'):
            classes.append(mktu.text.split('.')[0].replace('\n\t\t\t', ''))
        
        classes_numbers = []
        for number in classes:
            classes_numbers.append(number[:2])

        return classes, classes_numbers

    def get_unprotected(self) -> str:
        """
        Возвращает строку с неохраняемыми элементами товарного знака
        """

        for info in self.soup.find_all('p', class_='bib'):
            if '(526)' in info.text:
                return info.text.strip()[6:].split('\n')[2]

    def get_validity(self) -> str:
        """
        Возвращает дату истечения срока действия исключительного права
        """

        validity_list = []

        for info in self.soup.find_all('p', class_='bib'):
            if '(186)' in info.text:
                validity_list.append(info.text.strip()[-10:])

            if validity_list:
                validity = validity_list[-1]
            else:
                for info2 in self.soup.find('td', id='BibL').find_all('p'):
                    if '(181)' in info2.text:
                        validity = info2.text.strip()[-10:]
        
        return validity


class TMApplication(TradeMark):
    """
    Класс для получения информации со страницы заявки на товарный знак
    """

    def __init__(self, *args):
        super().__init__(*args)

    def get_status(self) -> str:
        """
        Возвращает строку с состоянием делопроизводства
        """

        info = self.soup.find('td', class_='White').text
        info = info.strip().split('\n\t\t\t\t')
        info = list(map(str.strip, info))
        return " ".join(info)

    def get_applicationdate(self) -> str:
        """
        Возвращает дату подачи заявки
        """

        for info in self.soup.find('td', id='BibL').find_all('p'):
            if '(220)' in info.text:
                return info.text.strip()[26:]

    def get_holder(self) -> str:
        """
        Возвращает имя заявителя
        """

        for info in self.soup.find_all('b'):
            if 'RU' in info.text:
                return info.text[4:] + '(RU)'

    """
    Переопределенные ниже методы заглушены,
    так как неактуальны для заявленных на
    регистрацию обозначений
    """
    def get_registrationdate(self):
        pass

    def get_unprotected(self):
        pass
    
    def get_validity(self):
        pass
